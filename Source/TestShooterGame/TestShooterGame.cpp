// Copyright Epic Games, Inc. All Rights Reserved.

#include "TestShooterGame.h"
#include "Modules/ModuleManager.h"

IMPLEMENT_PRIMARY_GAME_MODULE( FDefaultGameModuleImpl, TestShooterGame, "TestShooterGame" );

DEFINE_LOG_CATEGORY(LogTestShooterGame)
 